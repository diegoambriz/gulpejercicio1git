const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('prefix', () =>
    gulp.src('build/app.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
        }))
        .pipe(gulp.dest('dist')));

/////////////////// UGLIFY

let uglify = require('gulp-uglify');
let { pipeline } = require('readable-stream');

gulp.task('uglify1', () => {
    return pipeline(
        gulp.src('build/testUglify.js'),
        uglify(),
        gulp.dest('dist')
    );
});

/////////////////// HEADER
// assign the module to a local variable
let header = require('gulp-header');


// literal string
// NOTE: a line separator will not be added automatically
gulp.task('header1', () => {

    gulp.src('build/testHeader.js')
        .pipe(header('Hello'))
        .pipe(gulp.dest('./dist/'));
});
///////////////////////  HTMLMIN
const htmlmin = require('gulp-htmlmin');

gulp.task('minify', () => {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'));
});

/////////////////////// STRIP COMMENTS
let strip = require('gulp-strip-comments');

gulp.task('default', () => {
    return gulp.src('build/testStripcomments.js')
        .pipe(strip())
        .pipe(gulp.dest('dist'));
});
/////////////////////// CLEAN CSS
const cleanCSS = require('gulp-clean-css');

gulp.task('minify-css1', () => {
    return gulp.src('build/testCleancss.css')
        .pipe(cleanCSS({ debug: true }, (details) => {
            console.log(`${details.name}: ${details.stats.originalSize}`);
            console.log(`${details.name}: ${details.stats.minifiedSize}`);
        }))
        .pipe(gulp.dest('dist'));
});
/////////////////////// IMAGEMIN
const imagemin = require('gulp-imagemin');

gulp.task('imagemin1', () =>
    gulp.src('build/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images')),
);
///////////////////////  PUG
var pug = require('gulp-pug');
 
gulp.task('views', function buildHTML() {
  return gulp.src('views/*.pug')
  .pipe(pug({
    // Your options in here.
  }))
});
/////////////////////// DATA
var data = require('gulp-data');
var stylus = require('gulp-stylus');

gulp.task('one', function () {
    return gulp.src('./css/one.styl')
      .pipe(stylus())
      .pipe(gulp.dest('./css/build'));
  });
s
module.exports = gulp